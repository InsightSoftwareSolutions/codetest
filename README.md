# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
AGL code test - Asp.Net Core 2 web api (Additional task just to show back-end programming skills)
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Design patterns ###

Onion Architecture 
Data Repository pattern
DI
IOC

### Technologies used ###

Asp.Net Core 2.0
C#
Entity Framework Core 2.0
